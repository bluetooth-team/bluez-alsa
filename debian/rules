#!/usr/bin/make -f

# DEB_BUILD_PROFILES='nodoc'
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

export systemdsystemunitdir = $(shell pkg-config --variable=systemdsystemunitdir systemd | sed s,^/,,)

CONFIGURE_FLAGS := \
	--with-libfreeaptx \
	--enable-aptx \
	--enable-aptx-hd \
	--enable-faststream \
	--enable-lc3-swb \
	--enable-mp3lame \
	--enable-mpg123 \
	--enable-msbc \
	--enable-ofono \
	--enable-midi \
	--enable-opus \
	--enable-upower \
	--enable-cli \
	--enable-rfcomm \
	--enable-a2dpconf \
	--enable-hcitop \
	--enable-test \
	--enable-systemd \
	--enable-manpages \
	--enable-debug
# fdk-aac (libfdk-aac-dev) is in non-free
#	--enable-aac

ifeq ($(DEB_HOST_ARCH_ENDIAN),little)
CONFIGURE_FLAGS += --enable-ldac
endif

%:
	dh $@ --exclude=.la

override_dh_auto_configure:
	dh_auto_configure -- $(CONFIGURE_FLAGS)

execute_before_dh_install:
	# Remove static link libraries.
	rm -rf debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/alsa-lib/*.a

override_dh_installsystemd:
	dh_installsystemd -pbluez-alsa-utils --name=bluealsa-aplay bluealsa-aplay.service
	dh_installsystemd -pbluez-alsa-utils --name=bluealsa bluealsa.service

override_dh_auto_test:
	# DBus is needed to run the tests
ifneq (,$(filter $(DEB_BUILD_ARCH),mips64el loong64 hppa powerpc ppc64 sparc64 s390x ppc64el))
	dbus-run-session -- make CFLAGS="$(CFLAGS) -fno-lto" check VERBOSE=1 \
		|| echo "Ignoring test failures"
else
	dbus-run-session -- make CFLAGS="$(CFLAGS) -fno-lto" check VERBOSE=1
endif
